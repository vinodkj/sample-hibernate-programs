package wavelabs.ai;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

public class HibernateUtil {
	
	private static Configuration con=null;
	private static SessionFactory sf=null;
	private static Session session=null;
	
	
	public static Session getSession()
	{
		con=new Configuration().configure("hibernate.cfg.xml");
		sf=con.buildSessionFactory(new StandardServiceRegistryBuilder().configure().build());
		session=sf.openSession();
		return session;
		
	}
	
	public static void closeSession() {
		try {
		if(session!=null)
		{
			session.close();
			
		}
		}catch(HibernateException ex)
		{
			ex.getLocalizedMessage();		
			}
	}
	

}
