package wavelabs.ai;

public class Bike extends Vechile
{
	private String bikeName;
	private String model;
	
	public Bike()
	{
		
	}

	public String getBikeName() {
		return bikeName;
	}

	public void setBikeName(String bikeName) {
		this.bikeName = bikeName;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}
	
	

}
