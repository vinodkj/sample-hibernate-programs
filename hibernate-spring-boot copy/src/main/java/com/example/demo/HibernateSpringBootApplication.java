package com.example.demo;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.jboss.logging.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

import wavelabs.ai.Employee;
import wavelabs.ai.HibernateUtil;

@SpringBootApplication
@ComponentScan(basePackages = "wavelabs.ai")
public class HibernateSpringBootApplication {

	public static void main(String[] args) {
		SpringApplication.run(HibernateSpringBootApplication.class, args);
		Logger logger = Logger.getLogger(HibernateSpringBootApplication.class);

		logger.info("main method strated");
//		Student stu = new Student();
//		stu.setEmail("vinod@gmail.com");
//		stu.setName("vinod");
//		stu.setBlodGroup("o Positive");
//
//		Session session = HibernateUtil.getSession();
//
//		Transaction tx = session.beginTransaction();
//
//		session.save(stu);
//		tx.commit();
//
//		logger.info("----student object is saved-------");
//
//		Transaction tx2 = session.beginTransaction();
//
//		stu.setEmail("vino@care.com");
//		session.update(stu);
//		tx2.commit();
//
//		logger.info("---------student details updated");
//		
//		HibernateUtil.closeSession();
//
//		
//		session = HibernateUtil.getSession();
//	
//		Object obj = session.load(Student.class, new Integer(6));
//		
//		logger.info("object loaded from databases");
//		
//		Student stu2 = (Student) obj;
//
//		System.out.println("Id=" + stu2.getId());
//		System.out.println(" name=" + stu2.getName());
//		System.out.println("email=" + stu2.getEmail());
//		System.out.println("blood group=" + stu2.getBlodGroup());
//		
//		HibernateUtil.closeSession();
//		
//		
//		session=HibernateUtil.getSession();
//		tx=session.beginTransaction();
//		 stu=session.load(Student.class, new Integer(9));
//		 
//		 session.delete(stu);
//		 tx.commit();
//		 
//		 logger.info("object delated");
//
//		 
//		 HibernateUtil.closeSession();

		/*
		 * Bike bike = new Bike();
		 * 
		 * bike.setBikeName("ISMART"); bike.setModel("2018"); bike.setPrice(40000.00d);
		 * 
		 * Car car = new Car();
		 * 
		 * car.setName("Maruthi 800"); car.setType("non AC"); car.setPrice(800000.00d);
		 * 
		 * Session session = HibernateUtil.getSession();
		 * 
		 * Transaction tx = session.beginTransaction();
		 * 
		 * session.save(bike); session.save(car); tx.commit();
		 * HibernateUtil.closeSession();
		 */

		Employee emp = new Employee();
		emp.setName("vinod");
		emp.setEmail("vinod@care.com");
		emp.setBloodGroup("O positive");

		emp.setAddress("Hagalavadi Kurihalli");
		Session session=HibernateUtil.getSession();
	    Transaction tx=session.beginTransaction();
	    
	    session.save(emp);
	    tx.commit();
	    

	}

}
